using System.IO;
using UnityEngine;

namespace comguide.analytics
{
    public abstract class Logger : MonoBehaviour
    {
        private StreamWriter streamWriter;
        protected float startTime;
        protected const string DEL = ";";
        [SerializeField]
        protected string eventLogName = "eventLog";
        [SerializeField]
        private bool printToConsole = true;
        private bool init = false;

        public bool IsInit { get => init; }

        public void Init(string folderPath)
        {
            if (!IsInit)
            {
                init = true;
                startTime = Time.time;
                InitFiles(folderPath, eventLogName);
            }
            else
            {
                Debug.LogWarning("logger already init");
            }
        }

        public virtual void Stop()
        {
            init = false;
            streamWriter.Dispose();
            streamWriter = null;
        }


        /// <summary>
        /// inits stream writer
        /// </summary>
        /// <param name="folderName"></param>
        /// <param name="fileName"></param>
        private void InitFiles(string folderName, string fileName)
        {
            Directory.CreateDirectory(folderName);
            streamWriter = new StreamWriter(folderName + "/" + fileName + ".txt");
        }

        protected void WriteLine(string s)
        {
            if (printToConsole)
            {
                print(s);
            }
            if (init)
            {
                streamWriter.WriteLine(s);
            } else
            {
                //Debug.LogWarning("logger not initialized");
            }
        }

        protected void OnDestroy()
        {
            if (streamWriter != null)
            {
                streamWriter.Dispose();
            }
        }

        public float GetCurrentTime()
        {
            return (Time.time - startTime);
        }

        protected string GetFormattedTime()
        {
            return GetCurrentTime().ToString("F3");
        }

        protected string Format(Vector3 v)
        {
            return v.ToString("F2");
        }
    }
}
