
using System;

namespace comguide.analytics
{
    using LogStruct = Tuple<LogCategory, string>;
    public static class LogId
    {
        public static readonly LogStruct APP_QUIT = Tuple.Create(LogCategory.SYSTEM, "application_quit");
        public static readonly LogStruct SESSION_START = Tuple.Create(LogCategory.SYSTEM, "session_start");
        public static readonly LogStruct SESSION_FINISHED = Tuple.Create(LogCategory.SYSTEM, "session_finished");

        public static readonly LogStruct VIDEO_PLAYED = Tuple.Create(LogCategory.SYSTEM, "video_played");

        public static readonly LogStruct ANIMATOR_UNI_TRIGGERED = Tuple.Create(LogCategory.ANIMATOR, "animator_uni_triggered");
        public static readonly LogStruct ANIMATOR_REACTION_STARTED = Tuple.Create(LogCategory.ANIMATOR, "animator_reaction_started");
        public static readonly LogStruct ANIMATOR_REACTION_FINISHED = Tuple.Create(LogCategory.ANIMATOR, "animator_reaction_finished");
        public static readonly LogStruct ANIMATOR_LOOP_STARTED = Tuple.Create(LogCategory.ANIMATOR, "animator_loop_started");
        public static readonly LogStruct ANIMATOR_RESTARTED = Tuple.Create(LogCategory.ANIMATOR, "animator_restarted");
        public static readonly LogStruct ANIMATOR_FINISHED = Tuple.Create(LogCategory.ANIMATOR, "animator_finished");

        public static readonly LogStruct KEY_PRESSED = Tuple.Create(LogCategory.INPUT, "key_pressed");
        public static readonly LogStruct BUTTON_PRESSED = Tuple.Create(LogCategory.INPUT, "button_pressed");

        public static readonly LogStruct ERROR_ERROR = Tuple.Create(LogCategory.ERROR, "error");
        public static readonly LogStruct ERROR_EXCEPTION = Tuple.Create(LogCategory.ERROR, "exception");

    }

    public enum LogCategory
    {
        SYSTEM,
        INPUT,
        ANIMATOR,
        ERROR
    }
}
