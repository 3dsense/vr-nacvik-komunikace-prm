﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace comguide.analytics
{
    /// <summary>
    /// logging I/O logic using stream writers
    /// </summary>
    public class LoggingFileManager : MonoBehaviour
    {
        private StreamWriter streamWriter;

        // Start is called before the first frame update
        void Start()
        {

        }

        /// <summary>
        /// inits stream writer
        /// </summary>
        /// <param name="folderName"></param>
        /// <param name="fileName"></param>
        public void InitFiles(string folderName, string fileName)
        {
            Directory.CreateDirectory(folderName);
            streamWriter = new StreamWriter(folderName + "/" + fileName + ".txt");
        }

        public void WriteLine(string s)
        {
            streamWriter.WriteLine(s);
        }


        private void OnDestroy()
        {
            if (streamWriter != null)
            {
                streamWriter.Dispose();
            }
        }
    }
}