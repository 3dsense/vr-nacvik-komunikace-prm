﻿using System;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

namespace comguide.analytics
{
    /// <summary>
    /// manages gameplay logging
    /// </summary>
    public class EventLogger : Logger
    {
        private static EventLogger instance;
        public static EventLogger Get { get => GetInstance(); }

        private static EventLogger GetInstance()
        {
            if (instance == null)
            {
                var instances = FindObjectsOfType<EventLogger>();
                if (instances.Length == 0)
                {
                    Debug.LogError("no instance error");
                }
                else if (instances.Length > 1)
                {
                    Debug.LogError("multipla instances error");
                }
                else
                {
                    instance = instances[0];
                }
            }

            return instance;
        }

        public void Log(Tuple<LogCategory, string> logTuple, string param = "")
        {
            var t = GetFormattedTime();
            var log = t + DEL + logTuple.Item1.ToString() + DEL + logTuple.Item2 + DEL + param;
            WriteLine(log);
        }


    }
}