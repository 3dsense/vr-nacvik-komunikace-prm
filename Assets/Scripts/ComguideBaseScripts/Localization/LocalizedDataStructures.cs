using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace sense3d.localization
{
    public enum Localization
    {
        CZ = 0,
        EN = 1,
        NL = 2,
        DE = 3,
        PL = 4
    }

    /// <summary>
    /// serializable array used in localization system
    /// </summary>
    /// <typeparam name="T"></typeparam>
    [Serializable]
    public class LocalizedObjectArray<T>
    {
        [SerializeField]
        public List<LocalizedObject<T>> localizedObjects;

        public LocalizedObjectArray()
        {
            var enums = (Localization[])Enum.GetValues(typeof(Localization));
            localizedObjects = new List<LocalizedObject<T>>();
            for (int i = 0; i < enums.Length; i++)
            {
                Localization loc = enums[i];
                localizedObjects.Add(new LocalizedObject<T>(loc, default(T)));
            }
        }

        public void SetDefaultValue(T val)
        {
            foreach (var lO in localizedObjects)
            {
                lO.val = val;
            }
        }

        public void Add(Localization loc, T val)
        {
            var v = GetByLoc(loc);
            if (v == null)
            {
                localizedObjects.Add(new LocalizedObject<T>(loc, val));
            }
            else
            {
                v.val = val;
            }
        }

        public LocalizedObject<T> GetByLoc(Localization loc)
        {
            foreach (LocalizedObject<T> lO in localizedObjects)
            {
                if (lO != null && lO.key == loc)
                {
                    return lO;
                }
            }
            return null;
        }

        public T GetCurrentLocalized()
        {
            Localization loc;
            loc = default(Localization);
            loc = LocalizationManager.Get.CurrentLocalization;

            var locObj = GetByLoc(loc);
            if (locObj != null)
            {
                return GetByLoc(loc).val;
            }
            else
            {
                return default(T);
            }

        }
    }

    [Serializable]
    public class LocalizedObject<T>
    {
        public Localization key;
        public T val;

        public LocalizedObject(Localization key, T val)
        {
            this.key = key;
            this.val = val;
        }
    }

    public interface ILocalizable
    {
        public void Localize(Localization loc);
    }

}