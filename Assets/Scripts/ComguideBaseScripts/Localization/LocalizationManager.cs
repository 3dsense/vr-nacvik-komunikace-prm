using System.Collections.Generic;
using UnityEngine;

namespace sense3d.localization
{
    /// <summary>
    /// keeps current localization value
    /// manages all Localizables Components in the scene
    /// </summary>
    public class LocalizationManager : Singleton<LocalizationManager>
    {
        [SerializeField]
        private Localization currentLocalization;
        public List<ILocalizable> allLocalizables;

        public Localization CurrentLocalization { get => currentLocalization; }

        public delegate void LocChanged(Localization loc);
        public event LocChanged onLocalizationChanged;

        public void FindAllLocalizables()
        {
            allLocalizables = Helpers.Find<ILocalizable>(false);
            Localize();
        }

        public void Start()
        {
            FindAllLocalizables();
        }

        public void Localize()
        {
            foreach (var l in allLocalizables)
            {
                l.Localize(currentLocalization);
            }
        }

        public void ChangeLocalization(Localization loc)
        {
            currentLocalization = loc;
            FindAllLocalizables();
            onLocalizationChanged?.Invoke(currentLocalization);
        }

        public void ChangeLocalization(int loc)
        {
            ChangeLocalization((Localization)loc);
        }
    }

}
