using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace sense3d.localization
{
    [RequireComponent(typeof(Text))]
    public class LocalizedTextComponent : MonoBehaviour, ILocalizable
    {
        public LocalizedTextSO localizedTexts;

        public void OnEnable()
        {
            Localize(LocalizationManager.Get.CurrentLocalization);
        }

        public void Localize(Localization loc)
        {
            string str = loc + " no text";
            var t = localizedTexts.GetCurrentLocalized();
            GetComponent<Text>().text = t;
        }
    }
}

