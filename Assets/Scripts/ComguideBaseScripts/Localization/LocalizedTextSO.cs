using System;
using UnityEngine;


namespace sense3d.localization
{
    /// <summary>
    /// localized text scriptable object
    /// </summary>
    [CreateAssetMenu(fileName = "NewLocalizedText", menuName = "3dsense/CreateLocalizedTextSO", order = 0)]
    public class LocalizedTextSO : ScriptableObject
    {

        public bool formattedText = false;
        public LocalizedObjectArray<FormattedString> localizedTexts;

        public string GetCurrentLocalized()
        {
            var loc = localizedTexts.GetCurrentLocalized();
            if (loc == null)
            {
                return "missing text";
            }
            return localizedTexts.GetCurrentLocalized().text;
        }
    }

    [Serializable]
    public class FormattedString
    {
        [TextArea]
        public string text;

        public FormattedString(string text)
        {
            this.text = text;
        }
    }

}




