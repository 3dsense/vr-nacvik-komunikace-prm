using System;
using System.Collections.Generic;
using UnityEngine;

namespace comguide.animations
{
    /// <summary>
    /// simplifed controller built on the Mecanim system
    /// </summary>
    [RequireComponent(typeof(Animator)), Obsolete]
    public class SimplifiedMecanimAnimator : MonoBehaviour
    {
        private AnimatorOverrideController overrideController;
        public AnimationClip defaultAnimation;
        public Animator attachedAnimator;

        private string LOOP_ID = "loop";
        private string TRANSITION_ID = "transition";
        private AnimationClipOverrides clipOverrides;

        private void Awake()
        {
            overrideController = new AnimatorOverrideController(attachedAnimator.runtimeAnimatorController);
            attachedAnimator.runtimeAnimatorController = overrideController;

            foreach (var c in overrideController.animationClips)
            {
                print(c.name);
            }

            clipOverrides = new AnimationClipOverrides(overrideController.overridesCount);
            overrideController.GetOverrides(clipOverrides);
            SetLoopClip(defaultAnimation);
        }

        private void Update()
        {

        }

        public void SetTransitionClip(AnimationClip transitionClip)
        {
            clipOverrides[TRANSITION_ID] = transitionClip;
            overrideController.ApplyOverrides(clipOverrides);
        }

        public void SetLoopClip(AnimationClip loopClip)
        {
            print("setting loop to: " + loopClip.name);
            clipOverrides[LOOP_ID] = loopClip;
            overrideController.ApplyOverrides(clipOverrides);
        }


    }

    public class AnimationClipOverrides : List<KeyValuePair<AnimationClip, AnimationClip>>
    {
        public AnimationClipOverrides(int capacity) : base(capacity) { }

        public AnimationClip this[string name]
        {
            get { return this.Find(x => x.Key.name.Equals(name)).Value; }
            set
            {
                int index = this.FindIndex(x => x.Key.name.Equals(name));
                if (index != -1)
                    this[index] = new KeyValuePair<AnimationClip, AnimationClip>(this[index].Key, value);
            }
        }
    }

}