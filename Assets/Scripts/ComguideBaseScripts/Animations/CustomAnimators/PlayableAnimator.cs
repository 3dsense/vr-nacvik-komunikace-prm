using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Animations;
using UnityEngine.Audio;
using System;
using System.Collections;
using comguide.analytics;

namespace comguide.animations
{
    /// <summary>
    /// Custom animator based on the Unity's Playable system
    /// Plays animations on the Animator instead of animator controller
    /// </summary>
    [RequireComponent(typeof(Animator))]
    public class PlayableAnimator : ISimpleAnimator
    {
        /// <summary>
        /// playble graph structure
        /// </summary>
        private PlayableGraph playableGraph;
        private AnimationLayerMixerPlayable mainAnimationMixer;
        private AnimationMixerPlayable animationsMixer;
        private AnimationLayerMixerPlayable facialAnimationLayer;
        private AudioMixerPlayable audioMixerPlayable;
        private AnimationClipPlayable[] bodyAnimationsClips;
        private AudioClipPlayable[] audioClipPlayables;

        /// <summary>
        /// default animation played on startup
        /// </summary>
        [SerializeField]
        private AnimationClip defaultAnim;
        /// <summary>
        /// is animation currently looping
        /// </summary>
        private bool isLooping = true;

        /// <summary>
        /// current interpolation value between the 1th and 2nd animation layer
        /// used for animations transition
        /// weighted by 'transitionCurve'
        /// </summary>
        private float weight;

        /// <summary>
        /// current main animation layer index
        /// </summary>
        private int mainLayerIndex = 0;

        
        [SerializeField]
        private AnimationCurve transitionCurve = AnimationCurve.Linear(0, 0, 1, 1);
        [SerializeField]
        private float transitionLength = 1f;
        private float transitionTimer = 0;
        private bool inTransition = false;

        public delegate void OnTransitionFinished();
        public event OnTransitionFinished onTransitionFinished;

        /// <summary>
        /// facial audio layer index
        /// </summary>
        private readonly int facialAudioIndex = 2;
        /// <summary>
        /// foley audio layer index
        /// </summary>
        private readonly int foleyAudioIndex = 3;

        /// <summary>
        /// last received animation order request
        /// </summary>
        private AnimationBundle currentAnimationRequest;

        /// <summary>
        /// avatar mask used for facial masking
        /// </summary>
        [SerializeField]
        private AvatarMask facialMask;


        private Coroutine currentFacialAnimCoroutine;

        public float GetTransitionWeight()
        {
            return weight;
        }

        /// <summary>
        /// update playable graph based on new animation batch
        /// </summary>
        /// <param name="outputPlayable"></param>
        /// <param name="animationClip"></param>
        /// <param name="graphIndex"></param>
        /// <returns></returns>
        private AnimationClipPlayable CreateAnimationClipPlayable(Playable outputPlayable, AnimationClip animationClip, int graphIndex)
        {
            var oldPlayable = outputPlayable.GetInput(graphIndex);
            playableGraph.Disconnect(outputPlayable, graphIndex);
            var playable = AnimationClipPlayable.Create(playableGraph, animationClip);
            playableGraph.Connect(playable, 0, outputPlayable, graphIndex);
            playableGraph.SetTimeUpdateMode(DirectorUpdateMode.GameTime);
            playable.Pause();

            if (oldPlayable.IsValid())
            {
                oldPlayable.Destroy();
                //print("old playable destroyed");
            }

            return playable;
        }

        /// <summary>
        /// sets audioclip to desired audio layer
        /// </summary>
        /// <param name="layerIndex"></param>
        /// <param name="audioClip"></param>
        /// <param name="loop"></param>
        /// <returns></returns>
        private AudioClipPlayable SetAudio(int layerIndex, AudioClip audioClip, bool loop)
        {
            var audioPlayable = audioClipPlayables[layerIndex];
            audioPlayable.SetLooped(loop);
            audioPlayable.SetClip(audioClip);
            audioPlayable.SetTime(0);
            audioPlayable.Play();

            return audioPlayable;
        }


        /// <summary>
        /// generate audio layer and attach it into the playable graph
        /// </summary>
        /// <param name="layerIndex"></param>
        private void CreateAudioClipPlayable(int layerIndex)
        {
            var playable = AudioClipPlayable.Create(playableGraph, null, false);
            playable.Pause();
            playableGraph.Connect(playable, 0, audioMixerPlayable, layerIndex);
            audioClipPlayables[layerIndex] = playable;
        }

        void Awake()
        {
            // Creates the graph, the mixer and binds them to the Animator.
            playableGraph = PlayableGraph.Create("SimpleAnimator");

            //get audio source or create one if not existing
            var audioSource = GetComponentInChildren<AudioSource>();
            if (!audioSource)
            {
                Debug.LogError("no audio source attached in hiearrarchy, creating on in root");
                audioSource = gameObject.AddComponent<AudioSource>();
            }

            //generate the playable graph structure
            var audioOutput = AudioPlayableOutput.Create(playableGraph, "Audio", audioSource);
            var playableOutput = AnimationPlayableOutput.Create(playableGraph, "Animation", GetComponent<Animator>());


            mainAnimationMixer = AnimationLayerMixerPlayable.Create(playableGraph, 2);

            animationsMixer = AnimationMixerPlayable.Create(playableGraph, 2);
            facialAnimationLayer = AnimationLayerMixerPlayable.Create(playableGraph, 1);

            mainAnimationMixer.ConnectInput(0, animationsMixer, 0);
            mainAnimationMixer.ConnectInput(1, facialAnimationLayer, 0);

            facialAnimationLayer.SetLayerMaskFromAvatarMask(0, facialMask);
            facialAnimationLayer.SetInputWeight(0, 1f);


            playableOutput.SetSourcePlayable(mainAnimationMixer);
            CreateAnimationClipPlayable(facialAnimationLayer, defaultAnim, 0);


            // Creates AnimationClipPlayable and connects them to the mixer.
            bodyAnimationsClips = new AnimationClipPlayable[2];
            CreateAnimationClipPlayable(animationsMixer, defaultAnim, 0);
            CreateAnimationClipPlayable(animationsMixer, defaultAnim, 1);
            bodyAnimationsClips[0] = CreateAnimationClipPlayable(animationsMixer, defaultAnim, 0);
            bodyAnimationsClips[1] = CreateAnimationClipPlayable(animationsMixer, defaultAnim, 1);

            // audio nodes
            audioMixerPlayable = AudioMixerPlayable.Create(playableGraph, 4);
            audioClipPlayables = new AudioClipPlayable[4];
            CreateAudioClipPlayable(0);
            CreateAudioClipPlayable(1);
            CreateAudioClipPlayable(facialAudioIndex);
            CreateAudioClipPlayable(foleyAudioIndex);

            audioMixerPlayable.SetInputWeight(foleyAudioIndex, 1);


            mainAnimationMixer.SetInputWeight(0, 1);

            audioOutput.SetSourcePlayable(audioMixerPlayable);

            playableGraph.Play();
        }

        /// <summary>
        /// plays masked facial animation
        /// </summary>
        /// <param name="anim"></param>
        /// <param name="audio"></param>
        /// <param name="fade"></param>
        public void PlayFacialAnimation(AnimationClip anim, AudioClip audio, float fade = 0.25f)
        {
            if (currentFacialAnimCoroutine != null)
            {
                StopCoroutine(currentFacialAnimCoroutine);
            }
            print("playing facial animation");
            currentFacialAnimCoroutine = StartCoroutine(PlayFacialAnimationCoroutine(anim, audio, fade));
        }

        private void PlayFoley(AudioClip audio)
        {
            print("playing foley " + audio);
            var audioPlayable = SetAudio(foleyAudioIndex, audio, false);
            audioPlayable.Play();
        }


        private IEnumerator PlayFacialAnimationCoroutine(AnimationClip anim, AudioClip audio, float fade = 0.25f)
        {
            if (fade == 0)
            {
                fade = 0.001f;
            }
            float animLength = anim.length;
            var audioPlayable = SetAudio(facialAudioIndex, audio, false);
            var animationPlayable = CreateAnimationClipPlayable(facialAnimationLayer, anim, 0);
            audioMixerPlayable.SetInputWeight(facialAudioIndex, 1);
            animationPlayable.Play();
            audioPlayable.Play();

            float facialAnimLerp = 0f;
            while (animationPlayable.GetTime() < animLength)
            {
                if (animationPlayable.GetTime() < fade)
                {
                    facialAnimLerp += Time.deltaTime / fade;
                }
                else if (animLength - animationPlayable.GetTime() < fade)
                {
                    facialAnimLerp -= Time.deltaTime / fade;
                }
                else
                {
                    facialAnimLerp = 1;
                }
                facialAnimLerp = Mathf.Clamp01(facialAnimLerp);
                //print("lerp " + facialAnimLerp + " " + animationPlayable.GetTime() + " " + fade);

                mainAnimationMixer.SetInputWeight(1, facialAnimLerp);
                yield return null;
            }
            audioMixerPlayable.SetInputWeight(facialAudioIndex, 0);
            mainAnimationMixer.SetInputWeight(1, 0);
        }

        /// <summary>
        /// create new animation batch request
        /// </summary>
        /// <param name="anim">Unity animation clip</param>
        /// <param name="audio">Unity audio clip</param>
        /// <param name="loop">should loop?</param>
        /// <param name="transitionTime">fade time</param>
        public override void PlayAnimation(
            AnimationClip anim,
            AudioClip audio,
            bool loop,
            float transitionTime,
            AudioClip foley)
        {
            currentAnimationRequest = new AnimationBundle(anim, audio, loop, transitionTime, foley);
        }

        private int CurrentLayerIndex()
        {
            return mainLayerIndex;
        }

        private int NextLayerIndex()
        {
            return (mainLayerIndex + 1) % 2;
        }

        /// <summary>
        /// current body animation layer index
        /// </summary>
        /// <returns></returns>
        private AnimationClipPlayable CurrentBodyAnimation()
        {
            var p = bodyAnimationsClips[CurrentLayerIndex()];
            return p;
        }

        /// <summary>
        /// current audio animation layer index
        /// </summary>
        /// <returns></returns>
        private AudioClipPlayable CurrentAudioPlayable()
        {
            return audioClipPlayables[CurrentLayerIndex()];
        }


        /// <summary>
        /// trigger animation + audio based on last animation request
        /// </summary>
        private void HandleAnimationRequest()
        {
            if (currentAnimationRequest != null)
            {
                isLooping = currentAnimationRequest.loop;
                transitionLength = currentAnimationRequest.fadeLength;
                inTransition = true;
                transitionTimer = 0;
                print("transition started " + currentAnimationRequest.animationClip.name);
                var p = CreateAnimationClipPlayable(animationsMixer, currentAnimationRequest.animationClip, NextLayerIndex());
                bodyAnimationsClips[NextLayerIndex()] = p;

                SetAudio(NextLayerIndex(), currentAnimationRequest.audioClip, isLooping);
                p.Play();
                PlayFoley(currentAnimationRequest.foleyClip);
                currentAnimationRequest = null;
            }
        }

        public double GetCurrentAnimationTime()
        {
            var current = CurrentBodyAnimation();
            if (current.IsNull())
            {
                return 0;
            } 
            return CurrentBodyAnimation().GetTime();
        }

        public float GetTransitionTime()
        {
            return transitionTimer;
        }

        /// <summary>
        /// Handles animation layers loop
        /// </summary>
        private void HandleLooping()
        {
            //make loop
            if (CurrentBodyAnimation().GetAnimationClip() != null
                && CurrentBodyAnimation().GetTime() > CurrentBodyAnimation().GetAnimationClip().length
                && isLooping
                && !inTransition)
            {
                CurrentBodyAnimation().SetTime(0);
                CurrentAudioPlayable().SetTime(0);
                //print("loop point reached " + CurrentPlayble().GetAnimationClip().name);
            }

        }
        /// <summary>
        /// Handles proper layer indices and weights
        /// </summary>
        private void HandleTransition()
        {
            if (inTransition)
            {
                transitionTimer += Time.deltaTime;
                if (transitionTimer >= transitionLength)
                {
                    inTransition = false;
                    transitionTimer = 0;
                    mainLayerIndex = NextLayerIndex();

                    print("transition finished " + mainLayerIndex);
                    onTransitionFinished?.Invoke();
                }
            }

            float transitionRelativeTime = Mathf.Clamp01(transitionLength > 0 ? transitionTimer / transitionLength : 0);
            weight = transitionCurve.Evaluate(transitionRelativeTime);

            animationsMixer.SetInputWeight(CurrentLayerIndex(), 1.0f - weight);
            animationsMixer.SetInputWeight(NextLayerIndex(), weight);


            
            audioMixerPlayable.SetInputWeight(CurrentLayerIndex(), weight == 1 ? 0 : 1);
            audioMixerPlayable.SetInputWeight(NextLayerIndex(), weight == 0 ? 0 : 1);
        }


        void Update()
        {
            HandleAnimationRequest();

            HandleLooping();

            HandleTransition();
            
        }

        private void OnDestroy()
        {
            // Destroys all Playables and Outputs created by the graph.
            if (playableGraph.IsValid())
            {
                playableGraph.Destroy();
            }

        }

        void OnDisable()
        {

        }

        private void OnEnable()
        {
            HandleAnimationRequest();
        }


        /// <summary>
        /// seek animation time
        /// </summary>
        /// <param name="deltaTime"></param>
        public override void SeekRelative(float deltaTime)
        {
            var currentAnim = CurrentBodyAnimation();
            var currentTime = currentAnim.GetTime();
            var maxTime = currentAnim.GetAnimationClip().length;
            var newTime = Math.Clamp(currentTime + deltaTime, 0, maxTime);

            currentAnim.SetTime(newTime);
            CurrentAudioPlayable().SetTime(newTime);
        }

        /// <summary>
        /// used to store last animation request
        /// </summary>
        [Serializable]
        private class AnimationBundle
        {
            public AnimationClip animationClip;
            public AudioClip audioClip;
            public AudioClip foleyClip;
            public bool loop;
            public float fadeLength;

            public AnimationBundle(AnimationClip animationClip, AudioClip audioClip, bool loop, float fadeLength, AudioClip foley = null)
            {
                this.animationClip = animationClip;
                this.audioClip = audioClip;
                this.foleyClip = foley;
                this.loop = loop;
                this.fadeLength = fadeLength;
            }
        }

    }
}