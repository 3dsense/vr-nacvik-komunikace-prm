using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ISimpleAnimator : MonoBehaviour
{
    public abstract void PlayAnimation(
        AnimationClip anim, 
        AudioClip audio, 
        bool loop,
        float fadeTime,
        AudioClip foley = null);

    public abstract void SeekRelative(float deltaTime);

}

