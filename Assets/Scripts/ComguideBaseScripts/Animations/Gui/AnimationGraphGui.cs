﻿using sense3d.localization;
using System;
using UnityEngine;
using UnityEngine.UI;


namespace comguide.animations
{
    /// <summary>
    /// gui logic for animations graph
    /// </summary>
    public class AnimationGraphGui : MonoBehaviour, ILocalizable
    {
        [SerializeField]
        private AnimationGraphController animationController;
        public Button answerButtonPrefab;
        public Transform possibleAnswers;
        public Text animationInfo;

        public Button triggerButtonPrefab;

        public Color greenColor = Color.green;
        public Color yellowColor = Color.yellow;
        public Color orangeColor = new Color(1, 1, 0);
        public Color redColor = Color.red;

        [SerializeField]
        private GameObject debug;

        // Start is called before the first frame update
        void Start()
        {
            GenerateAnswersTriggers();
            GenerateTriggers();
            animationController.onIdleEnter += AnimationController_onIdleEnter;
            animationController.onAnimationPlayed += AnimationController_onAnimationPlayed;
            animationController.onRestarted += AnimationController_onRestarted;
            answerButtonPrefab.gameObject.SetActive(false);
            debug.SetActive(false);
        }

        public void SwitchDebugScreen()
        {
            ActivateDebugScreen(!debug.activeSelf);
        }

        public void ActivateDebugScreen(bool b)
        {
            debug.SetActive(b);
        }

        private void AnimationController_onRestarted()
        {
            GenerateAnswersTriggers();
        }

        private void AnimationController_onAnimationPlayed(AnimationNode node)
        {
            animationInfo.text = node.name + " " + node.GetCurrentAnimationIndex();
            foreach (Transform t in possibleAnswers)
            {
                var b = t.GetComponentInChildren<Button>();
                b.interactable = false;
            }
        }

        private void AnimationController_onIdleEnter()
        {
            GenerateAnswersTriggers();
        }

        public void Restart()
        {
            animationController.Restart();
        }

        /// <summary>
        /// creates button triggers for all nodes in library
        /// </summary>
        public void GenerateTriggers()
        {
            var availableTriggers = animationController.AnimationsLibrary.GetAnimationNodes();
            int counter = 0;
            var buttonsParent = triggerButtonPrefab.transform.parent;

            foreach (var animNode in availableTriggers)
            {
                bool isLooped = animNode is LoopedNode;
                var newButton = Instantiate(triggerButtonPrefab);
                newButton.gameObject.SetActive(true);
                var t = newButton.GetComponentInChildren<Text>();
                t.text = animNode.name;
                if (isLooped)
                {
                    t.text += " (L)";
                }
                newButton.image.color = GetNodeColor(animNode);

                int j = counter++;
                newButton.onClick.AddListener(delegate
                {
                    if (isLooped)
                    {
                        animationController.JumpToLoopedNode(animNode as LoopedNode);
                    }
                    else
                    {
                        animationController.JumpToReaction(animNode as ReactionNode);
                    }
                });
                newButton.transform.SetParent(buttonsParent);
                newButton.gameObject.SetActive(true);
                newButton.transform.localScale = Vector3.one;
                newButton.transform.localPosition = Vector3.zero;
                newButton.transform.localRotation = Quaternion.identity;
            }

            triggerButtonPrefab.gameObject.SetActive(false);
        }






    /// <summary>
    /// colors
    /// </summary>
    /// <param name="node"></param>
    /// <returns></returns>
    private Color GetNodeColor(AnimationNode node)
        {
            Color c = Color.white;
            if (node is LoopedNode)
            {
                return c;
            }

            ReactionNode n = node as ReactionNode;

            if (n.type == Reactiontype.POSITIVE || n.type == Reactiontype.GREEN)
            {
                c = greenColor;
            }
            else if (n.type == Reactiontype.NEGATIVE || n.type == Reactiontype.RED)
            {
                c = redColor;
            }
            else if (n.type == Reactiontype.ORANGE)
            {
                c = orangeColor;
            }
            else if (n.type == Reactiontype.YELLOW)
            {
                c = yellowColor;
            }
            return c;
        }

       

        /// <summary>
        /// creates button triggers for successing animation nodes
        /// </summary>
        public void GenerateAnswersTriggers()
        {
            DestroyChildren(possibleAnswers);


            var availableAnswers = animationController.GetCurrentlyPossibleSuccessors();
            if (availableAnswers == null)
            {
                animationInfo.text = "finish " + animationController.GetCurrentNode().name;
                return;
            }
            else
            {
                animationInfo.text = animationController.GetCurrentNode().name;
            }
            answerButtonPrefab.onClick.RemoveAllListeners();
            for (int i = 0; i < availableAnswers.Count; i++)
            {
                var answerNode = availableAnswers[i];
                var b = Instantiate(answerButtonPrefab);

                //is transition node
                bool isTransition = answerNode.IsTransitionNode();
                if (answerNode.IsTransitionNode())
                {
                    answerNode = answerNode.FollowingNode as ReactionNode;
                }

                var texts = b.GetComponentsInChildren<Text>();
                texts[0].text = answerNode.name + (isTransition ? "-t" : "");
                texts[1].text = answerNode.localizedLabels.GetCurrentLocalized().text;

                b.name = answerNode.name + "Button";
                int index = i;
                b.onClick.AddListener(delegate
                {
                    ButtonPressed(index);
                });
                b.transform.SetParent(possibleAnswers, false);
                b.gameObject.SetActive(true);
                b.image.color = GetNodeColor(answerNode);
            }

            answerButtonPrefab.gameObject.SetActive(false);
        }


        private void ButtonPressed(int index)
        {
            animationController.PlayReaction(index);
        }

        public void DestroyChildren(Transform transformObject)
        {
            foreach (Transform t in transformObject)
            {
                Destroy(t.gameObject);
            }
        }

        public void Localize(Localization loc)
        {
            GenerateAnswersTriggers();
        }
    }
}
