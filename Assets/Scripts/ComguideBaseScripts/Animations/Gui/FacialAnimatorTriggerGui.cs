using sense3d.localization;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


namespace comguide.animations
{
    /// <summary>
    /// gui logic for facial animations triggering
    /// </summary>
    public class FacialAnimatorTriggerGui : MonoBehaviour, ILocalizable
    {
        private Transform answersRoot;
        public Button buttonPrefab;
        public FacialAnimationsPlayer animationTriggerLogic;
        private List<Button> buttons;

        void Start()
        {
            buttons = new List<Button>();
            GenerateButtons();
            animationTriggerLogic.AnimController.onAnimationLibrarySet += GenerateButtons;

        }




        public void DestroyButtons()
        {
            for (int i = 0; i < buttons.Count; i++)
            {
                Button b = buttons[i];
                b.onClick.RemoveAllListeners();
                Destroy(b.gameObject);
            }
            buttons.Clear();
        }


        private void GenerateButtons()
        {
            DestroyButtons();
            answersRoot = buttonPrefab.transform.parent;
            var anims = animationTriggerLogic.GetUniversalReactionNodes();
            int counter = 0;
            foreach (var a in anims)
            {
                var newButton = Instantiate(buttonPrefab);
                newButton.gameObject.SetActive(true);
                int j = counter++;
                string prefix = "[" + (j + 1) + "]";
                newButton.GetComponentInChildren<Text>().text = a.localizedLabels.GetCurrentLocalized().text;
                newButton.onClick.AddListener(delegate
                {
                    bool wasTriggered = animationTriggerLogic.PlayAnimation(j);
                    if (wasTriggered)
                    {
                        animationTriggerLogic.onAnimationFinished += AnimationTriggerLogic_onAnimationFinished;
                        EnableButtons(false);
                    }
                });
                newButton.transform.SetParent(answersRoot);
                newButton.transform.localScale = Vector3.one;
                newButton.transform.localPosition = Vector3.zero;
                newButton.transform.localRotation = Quaternion.identity;
                buttons.Add(newButton);
            }

            buttonPrefab.gameObject.SetActive(false);
        }



        private void EnableButtons(bool e)
        {
            foreach (var b in buttons)
            {
                b.interactable = e;
            }
        }

        private void AnimationTriggerLogic_onAnimationFinished(UniversalReactionNode node)
        {
            EnableButtons(true);
        }





        // Update is called once per frame
        void Update()
        {

        }

        public void Localize(Localization loc)
        {
            var anims = animationTriggerLogic.GetUniversalReactionNodes();
            int index = 0;
            foreach (Transform b in buttonPrefab.transform.parent)
            {
                if (!b.gameObject.activeSelf) continue;
                b.GetComponentInChildren<Text>().text = anims[index].localizedLabels.GetCurrentLocalized().text;
                //print(index + " " + anims[index].localizedLabels.GetCurrentLocalized().text);
                index++;

            }
        }


    }
}
