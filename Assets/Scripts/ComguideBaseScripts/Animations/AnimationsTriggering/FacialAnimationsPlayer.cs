using comguide.analytics;
using sense3d.localization;
using System;
using System.Collections;
using UnityEngine;

namespace comguide.animations
{
    /// <summary>
    /// controls facial animation
    /// </summary>
    public class FacialAnimationsPlayer : MonoBehaviour
    {
        public delegate void AnimationFinished(UniversalReactionNode node);
        public event AnimationFinished onAnimationFinished;

        // Start is called before the first frame update
        [SerializeField]
        private AnimationGraphController animController;
        /// <summary>
        /// plays facial animation
        /// </summary>
        /// <param name="index"></param>
        ///

        private bool isInteractive = true;

        public bool IsInteractive { get => isInteractive;}
        public AnimationGraphController AnimController { get => animController; }

        public bool PlayAnimation(int index)
        {
            var uniReactions = GetUniversalReactionNodes();
            if (index >= uniReactions.Length) return false;

            if (!animController.CanBeTriggered())
            {
                Debug.Log("anim controller is playing another animation right now");
                return false;
            }

            isInteractive = false;

            var animator = animController.Animator as PlayableAnimator;
            var animNode = uniReactions[index];
            var localizedAnimation = animNode.GetNextRandomAnimation();
            animator.PlayFacialAnimation(localizedAnimation.GetAnimation(), localizedAnimation.GetAudio(), animNode.fadeLength);
            print("playing " + animNode.localizedLabels.GetCurrentLocalized().text);
            EventLogger.Get.Log(LogId.ANIMATOR_UNI_TRIGGERED, animNode.name + " " + animNode.GetCurrentAnimationIndex());
            StartCoroutine(TriggerFinishEvent(animNode));
            return true;
        }

        private IEnumerator TriggerFinishEvent(UniversalReactionNode anim)
        {
            yield return new WaitForSeconds(anim.GetCurrentAnimation().GetAnimation().length);
            isInteractive = true;
            onAnimationFinished?.Invoke(anim);
        }

        public UniversalReactionNode[] GetUniversalReactionNodes()
        {
            return animController.AnimationsLibrary.GetUniversalReactionNodes();
        }

    }

}
