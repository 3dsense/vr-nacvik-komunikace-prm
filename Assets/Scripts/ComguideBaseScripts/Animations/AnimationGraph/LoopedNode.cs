using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace comguide.animations
{
    /// <summary>
    /// looped animation node (idle state) represenation
    /// </summary>
    [CreateAssetMenu(fileName = "LoopedNode", menuName = "3dsense/CreateLoopedAnimation")]
    public class LoopedNode : AnimationNode
    {
        [SerializeField]
        private bool ignorePreviousNode = false;
        [SerializeField]
        private List<PossibleReactionNode> possibleSuccessingNodes;
        public List<PossibleReactionNode> PossibleSuccessingNodes { get => possibleSuccessingNodes; }
        /// <summary>
        /// get successing animation node corresponding to the previous one and provided index
        /// </summary>
        /// <param name="previousNode"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public ReactionNode GetSuccessingNode(ReactionNode previousNode, int index)
        {
            var successors = GetSuccessors(previousNode);
            if (successors != null)
            {
                return successors.GetSuccessingNodeByIndex(index);
            }
            return null;
        }

        /// <summary>
        /// get successing nodes corresponding to the previous one
        /// </summary>
        /// <param name="previousNode"></param>
        /// <returns></returns>
        public List<ReactionNode> GetSuccessingNodes(ReactionNode previousNode)
        {
            var successors = GetSuccessors(previousNode);
            if (successors != null)
            {
                return successors.Successors;
            }
            return null;
        }

        private PossibleReactionNode GetSuccessors(ReactionNode previousNode)
        {
            if (ignorePreviousNode)
            {
                if (possibleSuccessingNodes.Count < 0)
                {
                    return null;
                } else
                {
                    return possibleSuccessingNodes[0];
                }
            }
            
            foreach (var possibleSuccessors in possibleSuccessingNodes)
            {
                if (possibleSuccessors.PreviousNode == previousNode)
                {
                    return possibleSuccessors;
                }
            }
            return null;
        }

        /// <summary>
        /// get all successing nodes of given type
        /// </summary>
        /// <param name="previousNode"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public List<ReactionNode> GetSuccessingNodesOfType(ReactionNode previousNode, Reactiontype type)
        {
            var successors = GetSuccessors(previousNode);
            var successorsOfType = new List<ReactionNode>();
            if (successors != null)
            {
                foreach (var s in successors.Successors)
                {
                    if (type == s.type)
                    {
                        successorsOfType.Add(s);
                    }
                }
            }
            return successorsOfType;
        }


        public override List<AnimationNode> GetAllSuccessingNodes()
        {
            var allSuccessors = new List<AnimationNode>();
            foreach (var ps in possibleSuccessingNodes)
            {
                foreach (var s in ps.Successors)
                {
                    if (!allSuccessors.Contains(s))
                    {
                        allSuccessors.Add(s);
                    }
                }
            }
            return allSuccessors;
        }

    }


    /// <summary>
    /// is used to serialize the 'previous node - successing nodes' relation
    /// </summary>
    [Serializable]
    public class PossibleReactionNode
    {
        [SerializeField]
        private ReactionNode previousNode;
        [SerializeField]
        private List<ReactionNode> successors;

        public ReactionNode PreviousNode { get => previousNode; }
        public List<ReactionNode> Successors { get => successors; }

        public ReactionNode GetSuccessingNodeByIndex(int index)
        {
            if (successors.Count > index)
            {
                return successors[index];
            }
            return null;
        }

        /// <summary>
        /// emotional valu comparator
        /// </summary>
        public void OrderSuccessorsByType()
        {
            successors.Sort((s1, s2) => s1.CompareTo(s2));
        }
    }

#if UNITY_EDITOR
    /// <summary>
    /// custom inspector tool for successing nodes ordering based on its emotional value
    /// </summary>
    [UnityEditor.CustomEditor(typeof(LoopedNode)), UnityEditor.CanEditMultipleObjects]
    public class VideosManagerScriptEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            LoopedNode loopedNode = (LoopedNode)target;

            if (GUILayout.Button("order reactions"))
            {
                foreach (var ps in loopedNode.PossibleSuccessingNodes)
                {
                    Debug.Log(ps.PreviousNode);
                    ps.OrderSuccessorsByType();
                }
            }
            UnityEditor.EditorUtility.SetDirty(loopedNode);
            DrawDefaultInspector();
        }
    }

#endif

}