using UnityEngine;

namespace comguide.animations
{
    /// <summary>
    /// container for looped, reaction and universal animation nodes
    /// wraps animations graph
    /// serialized as scriptable object
    /// </summary>
    [CreateAssetMenu(fileName = "ReactionNode", menuName = "3dsense/CreateAnimationNodesLibrary")]
    public class AnimationNodesLibrary : ScriptableObject
    {
        [SerializeField]
        protected AnimationNode[] animationNodes;
        public virtual AnimationNode[] GetAnimationNodes()
        {
            return animationNodes;
        }

        [SerializeField]
        protected AnimationNode rootNode;
        public AnimationNode RootNode { get => rootNode; }

        [SerializeField]
        protected UniversalReactionNode[] universalReactions;
        public UniversalReactionNode[] GetUniversalReactionNodes()
        {
            return universalReactions;
        }

        public bool CheckReferences()
        {
            bool valid = true;
            foreach (var node in animationNodes)
            {
                foreach (var a in node.attachedAnimations)
                {
                    if (a.GetAnimation() == null)
                    {
                        Debug.LogError("missing reference " + node.name, node);
                        valid = false;
                    }

                    if (a.LocalizedAudios != null)
                    {
                        foreach (var sound in a.LocalizedAudios)
                        {
                            if (sound.audio == null)
                            {
                                Debug.LogWarning("missing audio " + node.name + sound.loc, node);
                            }
                        }
                    }
                }
            }
            return valid;
        }
    }

#if UNITY_EDITOR

    [UnityEditor.CustomEditor(typeof(AnimationNodesLibrary)), UnityEditor.CanEditMultipleObjects]
    public class AnimationNodesLibraryEditor : UnityEditor.Editor
    {
        public int callbackOrder => 0;

        public override void OnInspectorGUI()
        {
            if (GUILayout.Button("check references"))
            {
                AnimationNodesLibrary library = (AnimationNodesLibrary)target;
                library.CheckReferences();
            }
            DrawDefaultInspector();
        }

    }

#endif
}
