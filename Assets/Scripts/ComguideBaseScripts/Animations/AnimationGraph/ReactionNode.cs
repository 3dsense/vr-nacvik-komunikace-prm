using System.Collections.Generic;
using UnityEngine;


namespace comguide.animations
{
    /// <summary>
    /// non looped animation node (reaction)
    /// </summary>
    [CreateAssetMenu(fileName = "ReactionNode", menuName = "3dsense/CreateReactionAnimation")]
    public class ReactionNode : AnimationNode
    {
        /// <summary>
        /// reference to the successing animation node
        /// </summary>
        [SerializeField]
        private AnimationNode followingNode;

        public AnimationNode FollowingNode { get => followingNode; }
        public Reactiontype type;

        public override List<AnimationNode> GetAllSuccessingNodes()
        {
            var successors = new List<AnimationNode>();
            if (followingNode)
            {
                successors.Add(followingNode);
            }
            return successors;
        }

        public string GetFinalReactionName()
        {
            if (followingNode is ReactionNode)
            {
                return followingNode.name;
            }

            return name;
        }


        /// <summary>
        /// comparator based on emotional scale
        /// </summary>
        /// <param name="otherNode"></param>
        /// <returns></returns>
        public int CompareTo(ReactionNode otherNode)
        {
            Debug.Log(name + " " + this.type + " " + otherNode.type);
            if ((int)otherNode.type < (int)this.type)
            {
                return 1;
            }

            if ((int)otherNode.type == (int)this.type)
            {
                return this.name.CompareTo(otherNode.name);
            }

            return -1;
        }

        public bool IsTransitionNode()
        {
            return FollowingNode != null && FollowingNode is ReactionNode;
        }



    }
    /// <summary>
    /// reaction emotional scale
    /// </summary>
    public enum Reactiontype
    {
        POSITIVE,
        GREEN,
        YELLOW,
        ORANGE,
        RED,
        NEGATIVE,
        NONE
    }


}