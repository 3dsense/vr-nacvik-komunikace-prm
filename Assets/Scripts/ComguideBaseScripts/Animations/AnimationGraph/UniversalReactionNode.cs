using System.Collections.Generic;
using UnityEngine;


namespace comguide.animations
{
    /// <summary>
    /// universal reaction node used for the facial animation
    /// </summary>
    [CreateAssetMenu(fileName = "UniversalNode", menuName = "3dsense/CreateUniversalReaction")]
    public class UniversalReactionNode : AnimationNode
    {
        public float fadeLength = 0.25f;
        public override List<AnimationNode> GetAllSuccessingNodes()
        {
            return new List<AnimationNode>();
        }
    }
}
