using sense3d.localization;
using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace comguide.animations
{
    /// <summary>
    /// base class representing animation node
    /// serialized as scriptable object
    /// </summary>
    public abstract class AnimationNode : ScriptableObject
    {
        public List <LocalizedAnimation> attachedAnimations;
        private ShuffleList<LocalizedAnimation> shuffledAnimations;
        public AnimationCurve lookAtCurve;
        private LocalizedAnimation currentAnimation;
        [TextArea, Obsolete]
        private string label;
        public LocalizedObjectArray<FormattedString> localizedLabels;

        /// <summary>
        /// 
        /// </summary>
        /// <returns>currently used localized animation</returns>
        public LocalizedAnimation GetCurrentAnimation()
        {
            if (currentAnimation == null)
            {
                PrepareNextAnimation();
            }
            return currentAnimation;
        }

        public int GetCurrentAnimationIndex()
        {
            if (currentAnimation == null) return -1;
            return attachedAnimations.IndexOf(currentAnimation);
        }

        public LocalizedAnimation GetNextRandomAnimation()
        {
            PrepareNextAnimation();
            return currentAnimation;
        }

        private void PrepareNextAnimation()
        {
            if (shuffledAnimations == null)
            {
                shuffledAnimations = new ShuffleList<LocalizedAnimation>(attachedAnimations);
            }
            currentAnimation = shuffledAnimations.GetNextItem();
        }

        public abstract List<AnimationNode> GetAllSuccessingNodes();

    }

    /// <summary>
    /// wraps animation clip and adjacent localized audios
    /// </summary>
    [Serializable]
    public class LocalizedAnimation
    {
        [SerializeField]
        private AnimationClip animation;

        [SerializeField]
        private AudioClip foley;

        [SerializeField]
        private LocalizedAudio[] localizedAudios;
        public LocalizedAudio[] LocalizedAudios { get => localizedAudios; }

        public LocalizedAnimation(AnimationClip animation, LocalizedAudio[] audios)
        {
            this.animation = animation;
            localizedAudios = audios;
        }


        public AnimationClip GetAnimation()
        {
            return animation;
        }

        public AudioClip GetAudio()
        {
            foreach (var lA in localizedAudios)
            {
                if (lA.loc == LocalizationManager.Get.CurrentLocalization)
                {
                    return lA.audio;
                }
            }
            return null;
        }

        public AudioClip GetFoleyClip()
        {
            return foley;
        }


    }

    /// <summary>
    /// Localization, Audioclip pair
    /// </summary>
    [Serializable]
    public class LocalizedAudio
    {
        public Localization loc;
        public AudioClip audio;

        public LocalizedAudio(Localization lang, AudioClip audio)
        {
            this.loc = lang;
            this.audio = audio;
        }
    }


}