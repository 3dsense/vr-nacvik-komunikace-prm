using comguide.analytics;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace comguide.animations
{
    /// <summary>
    /// controller for an animation nodes graph
    /// </summary>
    public class AnimationGraphController : MonoBehaviour
    {
        [SerializeField]
        private FacialAnimationsPlayer uniReactionsPlayer;

        [SerializeField]
        private AnimationNodesLibrary animationsLibrary;
        public AnimationNodesLibrary AnimationsLibrary { get => animationsLibrary; }

        [SerializeField]
        private ISimpleAnimator animator;
        private LoopedNode currentNode;
        private ReactionNode previousNode;
        [SerializeField]
        private bool interactive = true;
        /// <summary>
        /// default animations blend time
        /// </summary>
        public float animationBlendTime = 1f;


        /// <summary>
        /// triggered when reaction has no successing looped node/>
        /// </summary>
        public event AnimatorFinish onFinish;
        public delegate void AnimatorFinish();

        /// <summary>
        /// true if there is no other animation to play
        /// </summary>
        private bool isFinished = false;
        public bool IsFinished { get => isFinished; }


        /// <summary>
        /// triggered when reaction has no successing looped node offset by animationFinishTimeOffset
        /// </summary>
        public event AnimationPathFinishing onFinishApproaching;
        public delegate void AnimationPathFinishing(float timeToFinish);
        public float animationFinishTimeOffset = 1f;


        /// <summary>
        /// triggered on Looped node entered
        /// </summary>
        public event AnimationLibrarySet onAnimationLibrarySet;
        public delegate void AnimationLibrarySet();

        /// <summary>
        /// triggered on Looped node entered
        /// </summary>
        public event LoopedStateEntered onIdleEnter;
        public delegate void LoopedStateEntered();

        /// <summary>
        /// triggered on restart -> root node entered
        /// </summary>
        public event Restarted onRestarted;
        public delegate void Restarted();

        /// <summary>
        /// triggered on animation node entered
        /// </summary>
        public event AnimationPlayedDel onAnimationPlayed;
        public delegate void AnimationPlayedDel(AnimationNode node);


        public bool CanBeTriggered()
        {
            return interactive && uniReactionsPlayer.IsInteractive;
        }
        public ISimpleAnimator Animator { get => animator; }

        /// <summary>
        /// character's init position/orientation
        /// </summary>
        [SerializeField]
        private Transform characterRoot;

        /// <summary>
        /// gets all reachable nodes from given animation node
        /// </summary>
        /// <param name="enterNode"></param>
        /// <returns></returns>
        public List<AnimationNode> GetAllSuccessingNodesRecursively(AnimationNode enterNode)
        {
            Stack<AnimationNode> nodesToTraverse = new Stack<AnimationNode>();
            HashSet<AnimationNode> traversedNodes = new HashSet<AnimationNode>();
            nodesToTraverse.Push(enterNode);
            while (nodesToTraverse.Count > 0)
            {
                var n = nodesToTraverse.Pop();
                traversedNodes.Add(n);

                foreach (var s in n.GetAllSuccessingNodes())
                {
                    if (!traversedNodes.Contains(s))
                    {
                        nodesToTraverse.Push(s);
                    }
                }
            }
            return new List<AnimationNode>(traversedNodes);
        }

        public void SetAnimationLibrary(AnimationNodesLibrary nodesLibrary)
        {
            animationsLibrary = nodesLibrary;
            onAnimationLibrarySet?.Invoke();
            print("animation library set " + nodesLibrary.name) ;
            Init();
        }

        /// <summary>
        /// sets to root animation node
        /// </summary>
        public void Init()
        {

            StopAllCoroutines();
            currentNode = animationsLibrary.RootNode as LoopedNode;
            previousNode = null;

            float blendTime = animationBlendTime;
            animationBlendTime = 0;
            PlayAnimation(currentNode, true);
            onIdleEnter?.Invoke();
            animationBlendTime = blendTime;

            EnableInteraction(true);
            isFinished = false;
            animator.transform.position = characterRoot.transform.position;
            animator.transform.rotation = characterRoot.transform.rotation;
            
        }

        public void Restart()
        {
            Init();
            EventLogger.Get.Log(LogId.ANIMATOR_RESTARTED);
            onRestarted?.Invoke();
        }

        private void Awake()
        {
            if (!uniReactionsPlayer)
            {
                uniReactionsPlayer = FindObjectOfType<FacialAnimationsPlayer>();
            }
            Init();
        }

        /// <summary>
        /// gets direct successors of last played looped node
        /// </summary>
        /// <returns></returns>
        public List<ReactionNode> GetCurrentlyPossibleSuccessors()
        {
            return currentNode.GetSuccessingNodes(previousNode);
        }

        /// <summary>
        /// plays random direct reaction
        /// </summary>
        /// <param name="type"></param>
        public void PlayRandomReaction()
        {
            if (CanBeTriggered())
            {
                EnableInteraction(false);
                var reactionNodes = GetCurrentlyPossibleSuccessors();
                if (reactionNodes != null)
                {
                    var reactionNode = reactionNodes[UnityEngine.Random.Range(0, reactionNodes.Count)];
                    StartCoroutine(PlayReactionCoroutine(reactionNode));
                }
            }
        }

        /// <summary>
        /// plays random direct reaction of given emotional type
        /// </summary>
        /// <param name="type"></param>
        public void PlayRandomReactionOfType(Reactiontype type, bool playAnyIfMissing = true)
        {
            print("playing random reaction " + type);
            if (CanBeTriggered())
            {
                var reactionNodes = currentNode.GetSuccessingNodesOfType(previousNode, type);
                if (reactionNodes.Count > 0)
                {
                    var reactionNode = reactionNodes[UnityEngine.Random.Range(0, reactionNodes.Count)];
                    EnableInteraction(false);
                    StartCoroutine(PlayReactionCoroutine(reactionNode));
                }
                else
                {
                    Debug.LogWarning("no successor found of " + type);
                    if (playAnyIfMissing)
                    {
                        PlayRandomReaction();
                    }
                }
            }
        }

        /// <summary>
        /// plays number of direct reaction of given emotional type
        /// </summary>
        /// <param name="type"></param>
        public int ReactionsOfTypeCount(Reactiontype type)
        {
            return currentNode.GetSuccessingNodesOfType(previousNode, type).Count;
        }

        /// <summary>
        /// plays 
        /// </summary>
        /// <param name="reactionNode"></param>
        private void PlayReaction(ReactionNode reactionNode)
        {
            if (CanBeTriggered())
            {
                EnableInteraction(false);
                StartCoroutine(PlayReactionCoroutine(reactionNode));
            }
        }


        private void EnableInteraction(bool b)
        {
            interactive = b;
            print("interaction " + b);
        }


        public AnimationNode GetCurrentNode()
        {
            return currentNode;
        }

        /// <summary>
        /// animation fast forwarding
        /// uses global time scaling
        /// </summary>
        /// <param name="timescale"></param>
        public void FastForward(float timescale)
        {
            Time.timeScale = timescale;
        }


        /// <summary>
        /// plays reaction node if possible
        /// </summary>
        /// <param name="index"></param>
        public void PlayReaction(int index)
        {
            if (CanBeTriggered())
            {
                var reactionNode = currentNode.GetSuccessingNode(previousNode, index);
                if (reactionNode)
                {
                    PlayReaction(reactionNode);
                }
                else
                {
                    Debug.LogWarning("no successor found");
                }
            }
        }

        public ReactionNode GetLastReactionNode()
        {
            return previousNode;
        }

        /// <summary>
        /// force jump to reaction node
        /// </summary>
        public void JumpToReaction(ReactionNode node)
        {
            StopAllCoroutines();
            EnableInteraction(true);
            PlayReaction(node);
        }

        /// <summary>
        /// force jump to looped node
        /// </summary>
        /// <param name="node"></param>
        public void JumpToLoopedNode(LoopedNode node)
        {
            print("jump to " + node.name);
            previousNode = null;
            currentNode = node;
            EnableInteraction(true);

            var localizedAnim = node.GetNextRandomAnimation();
            animator.PlayAnimation(localizedAnim.GetAnimation(), localizedAnim.GetAudio(), true, 0, localizedAnim.GetFoleyClip());
            onIdleEnter?.Invoke();
            onAnimationPlayed?.Invoke(node);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="node">animation node</param>
        /// <param name="loop">should loop</param>
        private void PlayAnimation(AnimationNode node, bool loop)
        {
            print("playing " + node.name);
            var localizedAnim = node.GetNextRandomAnimation();
            var animationLength = localizedAnim.GetAnimation().length;
            if (loop)
            {
                EventLogger.Get.Log(LogId.ANIMATOR_LOOP_STARTED, node.name + " " + node.GetCurrentAnimationIndex());
            }
            else
            {
                EventLogger.Get.Log(LogId.ANIMATOR_REACTION_STARTED, node.name + " " + node.GetCurrentAnimationIndex());
            }
            var blendTime = Mathf.Min(animationLength / 2, animationBlendTime);

            animator.PlayAnimation(localizedAnim.GetAnimation(), localizedAnim.GetAudio(), loop, blendTime, localizedAnim.GetFoleyClip());
            onAnimationPlayed?.Invoke(node);
        }


        /// <summary>
        /// plays reaction and successing looped node
        /// </summary>
        /// <param name="reactionNode"></param>
        /// <returns></returns>
        private IEnumerator PlayReactionCoroutine(ReactionNode reactionNode)
        {
            PlayAnimation(reactionNode, false);
            var nextNode = reactionNode.FollowingNode;

            var animationLength = reactionNode.GetCurrentAnimation().GetAnimation().length;
            var blendTime = Mathf.Min(animationLength / 2, animationBlendTime);

            var playTime = animationLength - blendTime;
            previousNode = reactionNode;

            if (nextNode == null)
            {
                yield return new WaitForSeconds(playTime - animationFinishTimeOffset);
                onFinishApproaching?.Invoke(animationFinishTimeOffset);
                yield return new WaitForSeconds(animationFinishTimeOffset);
                EnableInteraction(true);
                isFinished = true;
                EventLogger.Get.Log(LogId.ANIMATOR_FINISHED);
                onFinish?.Invoke();
            }
            else
            {
                yield return new WaitForSeconds(playTime);
                if (nextNode is LoopedNode)
                {
                    currentNode = nextNode as LoopedNode;
                    PlayAnimation(currentNode, true);

                    yield return new WaitForSeconds(blendTime);
                    EnableInteraction(true);
                    onIdleEnter?.Invoke();
                }
                else
                {
                    previousNode = nextNode as ReactionNode;
                    StartCoroutine(PlayReactionCoroutine(nextNode as ReactionNode));
                }
                yield return new WaitForSeconds(blendTime);
                //animationStopped
                EventLogger.Get.Log(LogId.ANIMATOR_REACTION_FINISHED, reactionNode.name);
            }
        }
    }
}