using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShuffleList <T>
{
    private List<T> items;
    private List<T> shuffledItems;

    public ShuffleList(List<T> items)
    {
        this.items = items;
        shuffledItems = new List<T>();
    }

    public T GetNextItem()
    {
        if (shuffledItems.Count == 0)
        {
            shuffledItems.AddRange(items);
        }
        var index = Random.Range(0, shuffledItems.Count);
        var item = shuffledItems[index];
        shuffledItems.RemoveAt(index);
        return item;
    }
}
