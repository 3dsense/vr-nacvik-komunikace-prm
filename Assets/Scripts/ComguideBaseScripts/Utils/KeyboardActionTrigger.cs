﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

public class KeyboardActionTrigger : MonoBehaviour
{
    public List<KeyboardAction> actions;

    // Start is called before the first frame update
    void Start()
    {

    }

    public override string ToString()
    {
        var sb = new StringBuilder();
        foreach (var a in actions)
        {
            sb.AppendLine(a.description + " [" + a.key + "]");
        }

        return sb.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        if (actions != null)
        {
            foreach (var a in actions)
            {
                if ((Input.GetKey(a.key) && a.inputType == InputType.Key)
                    || (Input.GetKeyDown(a.key) && a.inputType == InputType.KeyDown)
                    || (Input.GetKeyUp(a.key) && a.inputType == InputType.KeyUp))
                {
                    if (a.action != null)
                    {
                        a.action.Invoke();
                        print("keyboard trigger action " + a.inputType + " " + a.key);
                    }
                }
            }
        }
    }


    [System.Serializable]
    public class KeyboardAction
    {
        public KeyCode key;
        public InputType inputType = InputType.KeyDown;
        public UnityEvent action;
        [TextArea]
        public string description;

    }

    public enum InputType
    {
        Key, KeyDown, KeyUp
    }

#if UNITY_EDITOR
    /// <summary>
    /// custom inspector tool for successing nodes ordering based on its emotional value
    /// </summary>
    [UnityEditor.CustomEditor(typeof(KeyboardActionTrigger))]
    public class KeyboardActionTriggerEditor : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            KeyboardActionTrigger trigger = (KeyboardActionTrigger)target;

            if (GUILayout.Button("trigger all actions"))
            {
                foreach (var a in trigger.actions)
                {
                    a.action.Invoke();
                }
            }
        }
    }

#endif


}
