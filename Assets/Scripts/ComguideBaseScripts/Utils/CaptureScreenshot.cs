﻿using System;
using UnityEngine;

public class CaptureScreenshot : MonoBehaviour
{
    public KeyCode key = KeyCode.S;
    
    private void Update()
    {
        if (Input.GetKeyDown(key))
        {

            ScreenCapture.CaptureScreenshot("Screenshot_" + DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss") + ".png" );
            print("screenshot captured");
        }
    }
}