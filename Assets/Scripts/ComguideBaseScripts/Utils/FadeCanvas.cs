﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;
using System;
[RequireComponent(typeof(CanvasGroup))]
public class FadeCanvas : MonoBehaviour
{
    private CanvasGroup canvasGroup;
    public bool visible = true;
    public float Alpha { get => alpha; }
    private float alpha = 0;
    private float prevAlpha = 0;
    public float fadeTime = 1;
    [SerializeField]
    private float initAlpha = 1;
    [SerializeField]
    public bool deactivateOnFadeFinished;


    public delegate void FadeFinished(float alpha);
    public event FadeFinished onFadeFinished;

    // Use this for initialization
    void Awake()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        alpha = initAlpha;
        prevAlpha = alpha;
        SetColor();
    }

    public void SetCurrentAlpha(float v, bool visible)
    {
        alpha = v;
        prevAlpha = v;
        this.visible = visible;
        SetColor();
    }

    public bool IsInteractable()
    {
        return canvasGroup.interactable;
    }

    // Update is called once per frame
    void Update()
    {
        
        float delta = 1/fadeTime * Time.deltaTime;
        alpha += visible ? delta : -delta;
        alpha = Mathf.Clamp(alpha, 0, 1);
        SetColor();
        //canvasGroup.interactable = alpha == 1;
        canvasGroup.blocksRaycasts = alpha > 0;


        if (alpha != prevAlpha)
        {
            if (alpha == 1)
            {
                onFadeFinished?.Invoke(1);
            } else if (alpha == 0){

                onFadeFinished?.Invoke(0);
                if (deactivateOnFadeFinished)
                {
                    gameObject.SetActive(false);
                }
            }
        }

        prevAlpha = alpha;
        
    }

    private void SetColor()
    {
        canvasGroup = GetComponent<CanvasGroup>();
        canvasGroup.alpha = alpha;
    }

    public void SetVisible(bool b)
    {
        visible = b;
    }
}
