using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// follows given target with world space offset
/// commonly attached to the camera and used as character's look at target
/// </summary>
public class FollowTarget : MonoBehaviour
{
    public Vector3 offset;
    public Transform toFollow;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = toFollow.position + offset;
    }
}
