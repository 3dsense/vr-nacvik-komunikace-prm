using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class CopyBlendShapesValues : MonoBehaviour
{
    public SkinnedMeshRenderer targetRenderer;
    private SkinnedMeshRenderer r;
    public bool overrideEnabled = true;
    // Start is called before the first frame update
    void Start()
    {
        r = GetComponent<SkinnedMeshRenderer>();
    }

    // Update is called once per frame
    void LateUpdate()
    {
        for (int i = 0; i < targetRenderer.sharedMesh.blendShapeCount; i++)
        {
            var bsName = targetRenderer.sharedMesh.GetBlendShapeName(i);
            var val = targetRenderer.GetBlendShapeWeight(i);
            var index = r.sharedMesh.GetBlendShapeIndex(bsName);

            if (index > 0)
            {
                r.SetBlendShapeWeight(index, Mathf.Clamp(val, 0, 100));
            }
        }
        if (overrideEnabled)
        {
            r.enabled = true;
        }


    }


}
