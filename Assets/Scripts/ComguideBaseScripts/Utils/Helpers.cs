using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/// <summary>
/// static class with helper functions
/// </summary>
public static class Helpers
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T">component type</typeparam>
    /// <returns>list of all componenets in the scene</returns>
    public static List<T> Find<T>(bool includingInactive = true)
    {
        List<T> interfaces = new List<T>();
        GameObject[] rootGameObjects = SceneManager.GetActiveScene().GetRootGameObjects();
        foreach (var rootGameObject in rootGameObjects)
        {
            T[] childrenInterfaces = rootGameObject.GetComponentsInChildren<T>(includingInactive);
            foreach (var childInterface in childrenInterfaces)
            {
                interfaces.Add(childInterface);
            }
        }
        return interfaces;
    }

    public static Transform CopyTransformValues(Transform from, Transform to)
    {
        to.localScale = from.localScale;
        to.localRotation = from.localRotation;
        to.localPosition = from.localPosition;

        return to;
    }
    public static RectTransform CopyRectTransform(RectTransform from, RectTransform to)
    {
        to.anchorMin = from.anchorMin;
        to.anchorMax = from.anchorMax;
        to.anchoredPosition = from.anchoredPosition;
        to.sizeDelta = from.sizeDelta;
        to.localScale = from.localScale;

        return to;
    }

    public static string GetButtonCaption(Button button)
    {
        var caption = button.name;
        var text = button.GetComponentInChildren<Text>();
        if (text)
        {
            caption = text.text;
        }
        return caption;
    }


}