using UnityEngine;
using UnityEngine.Rendering;

public abstract class PostprocessFade : MonoBehaviour
{
    protected bool faded = false;
    protected bool fadeFinished = true;

    public bool IsFaded { get => faded; }

    public abstract void SetFaded(bool faded);

    public delegate void FadeFinishedDel(bool faded);
    public event FadeFinishedDel onFadeFinished;

    protected void RaiseFadeFinishedEvent(bool b)
    {
        onFadeFinished?.Invoke(b);
    }

    public abstract float GetNormalizedProgess();
}
