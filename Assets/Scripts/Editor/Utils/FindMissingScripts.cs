using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

public class FindMissingScripts : EditorWindow
{
    [MenuItem("Tools/Find Missing Scripts")]
    private static void ShowWindow()
    {
        var window = GetWindow<FindMissingScripts>();
        window.titleContent = new GUIContent("Find Missing Scripts");
        window.Show();
    }

    private void OnGUI()
    {
        if (GUILayout.Button("Find Missing Scripts in Scene"))
        {
            FindInScene();
        }
    }

    private static void FindInScene()
    {
        GameObject[] allObjects = GameObject.FindObjectsOfType<GameObject>();
        List<GameObject> objectsWithMissingScripts = new List<GameObject>();

        foreach (GameObject go in allObjects)
        {
            Component[] components = go.GetComponents<Component>();
            foreach (Component component in components)
            {
                if (component == null)
                {
                    objectsWithMissingScripts.Add(go);
                    break;
                }
            }
        }

        if (objectsWithMissingScripts.Count > 0)
        {
            Debug.Log("Objects with missing scripts found:");
            foreach (GameObject go in objectsWithMissingScripts)
            {
                Debug.Log(go.name, go);
            }
        }
        else
        {
            Debug.Log("No GameObjects with missing scripts found in the scene.");
        }
    }
}
