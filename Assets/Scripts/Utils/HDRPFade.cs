using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.HighDefinition;

public class HDRPFade : PostprocessFade
{
    private Exposure exposureComponent;
    public float savedValue = 1f;
    public float subtractCoef = 10f;
    public float fadeSpeed = 1f;
    public Volume postProcessVolume;


    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        postProcessVolume.sharedProfile.TryGet(out exposureComponent);
        float targetValue = faded ? (savedValue + subtractCoef) : savedValue;
        float currentValue = exposureComponent.fixedExposure.value;
        float updatedValue = Mathf.MoveTowards(currentValue, targetValue, Time.deltaTime * (subtractCoef * fadeSpeed));
        exposureComponent.fixedExposure.value = updatedValue;
        if (!fadeFinished)
        {
            if (Equals(updatedValue, savedValue))
            {
                fadeFinished = true;
                RaiseFadeFinishedEvent(false);
            }
            if (Equals(updatedValue, savedValue + subtractCoef))
            {
                fadeFinished = true;
                RaiseFadeFinishedEvent(true);
            }
        }

    }

    private void OnDestroy()
    {
        exposureComponent.fixedExposure.value = savedValue;
    }

    public override void SetFaded(bool faded)
    {
        this.faded = faded;
        fadeFinished = false;
    }

    public override float GetNormalizedProgess()
    {
        float currentValue = exposureComponent.fixedExposure.value;
        return Mathf.InverseLerp(savedValue, savedValue + subtractCoef, currentValue);
    }
}
