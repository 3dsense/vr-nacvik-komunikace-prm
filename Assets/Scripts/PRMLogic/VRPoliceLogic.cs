using comguide.animations;
using System;
using UnityEngine;

namespace comguide.logic.vrpolice
{
    public class VRPoliceLogic : MonoBehaviour
    {
        public AnimationGraphController animator;
        public FacialAnimationsPlayer facialAnimator;
        public FadeCanvas fade;
        public VRPoliceLogger logger;
        private void Start()
        {
            animator.onFinishApproaching += Animator_onFinishApproaching;
            animator.onFinish += () => logger.LogState("END", "-", "-");
            logger.LogState("START", "-", "-");
        }

        private void Animator_onFinishApproaching(float timeToFinish)
        {
            fade.visible = true;
        }

        public void PlayFacialAnimation(int index)
        {
            facialAnimator.PlayAnimation(index);
        }

        private string GetLastNodeString()
        {
            var lastReaction = animator.GetLastReactionNode();
            if (lastReaction)
            {
                return lastReaction.name;
            }
            return "";
        }

        public void PlayReaction(bool good)
        {
            animator.PlayRandomReactionOfType(good ? Reactiontype.POSITIVE : Reactiontype.NEGATIVE);
            logger.LogState(GetLastNodeString(), "-", good ? "Good" : "Bad");
            if (good) { logger.IncreaseBad(); } else { logger.IncreaseBad(); }

        }


        internal void PlayGoodReaction()
        {
            if (animator.CanBeTriggered())
            {
                PlayReaction(true);
            }
        }

        internal void PlayBadReaction()
        {

            if (animator.CanBeTriggered())
            {
                PlayReaction(false);
            }
        }

        public void Restart()
        {
            if (fade.Alpha < 1)
            {
                fade.visible = true;
                fade.onFadeFinished += Fade_onFadeFinished;
            }
            else
            {
                RestartGame();
            }
        }

        private void RestartGame()
        {
            animator.Restart();
            logger.ResetStats();
            logger.LogState("START", "-", "-");
            fade.visible = false;

        }

        private void Fade_onFadeFinished(float alpha)
        {
            fade.onFadeFinished -= Fade_onFadeFinished;
            if (alpha == 1)
            {
                RestartGame();
            }

        }
    }
}