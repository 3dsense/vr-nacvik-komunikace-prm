using comguide.animations;
using System;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

namespace comguide.logic.vrpolice
{
    public class VRPoliceGuiLogic : MonoBehaviour
    {
        public InputActionReference escalate;
        public InputActionReference deescalate;
        public InputActionReference reset;
        public VRPoliceLogic policeLogic;

        public Transform reactionsTransform;
        public GameObject idleActiveCaption;
        public Text info;

        public InputActionReference [] uniReactions;


        private void Start()
        {
            policeLogic.animator.onFinish += ()=> info.text = "END";
            policeLogic.animator.onIdleEnter += Animator_onIdleEnter;
            policeLogic.animator.onAnimationPlayed += Animator_onAnimationPlayed;
            escalate.action.performed += escalatePressed;
            deescalate.action.performed += deescalatePressed;
            reset.action.performed += resetPressed;
            for (int i = 0; i < policeLogic.facialAnimator.GetUniversalReactionNodes().Length; i++)
            {
                var r = uniReactions[i];
                int index = i;
                r.action.performed += ctx => policeLogic.PlayFacialAnimation(index);
            }
            GenerateReactionsGui();
        }

        private void Animator_onAnimationPlayed(animations.AnimationNode node)
        {
            if (node is ReactionNode)
            {
                info.text = node.name;
            }
        }


        private void GenerateReactionsGui()
        {
            var prefab = reactionsTransform.GetChild(0);
            var anims = policeLogic.facialAnimator.GetUniversalReactionNodes();
            for (int i = 0; i < anims.Length; i++)
            {
                var a = anims[i];
                var guiLine = Instantiate(prefab);
                var texts = guiLine.GetComponentsInChildren<Text>();
                texts[0].text = (i + 1).ToString();
                texts[1].text = a.localizedLabels.GetCurrentLocalized().text;
                guiLine.transform.SetParent(reactionsTransform);
            }
            prefab.gameObject.SetActive(false);
        }

        private void resetPressed(InputAction.CallbackContext obj)
        {
            policeLogic.Restart();
            idleActiveCaption.gameObject.SetActive(false);
            info.text = "START";

        }

        private void Animator_onIdleEnter()
        {
            idleActiveCaption.gameObject.SetActive(true);
        }

        private void deescalatePressed(InputAction.CallbackContext obj)
        {
            policeLogic.PlayGoodReaction();
            idleActiveCaption.gameObject.SetActive(false);

        }

        private void escalatePressed(InputAction.CallbackContext obj)
        {
            policeLogic.PlayBadReaction();
            idleActiveCaption.gameObject.SetActive(false);

        }


    }
}
