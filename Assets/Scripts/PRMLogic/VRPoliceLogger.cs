using comguide.analytics;
using System;
using System.IO;
using UnityEngine;

namespace comguide.logic.vrpolice
{
    public class VRPoliceLogger : MonoBehaviour
    {
        private int totalGood;
        private int totalBad;

        public ParametersManager parameters;
        private string sessionId;
        private void Awake()
        {
            sessionId = DateTime.Now.ToString("yyyyMMddHHmm");
            ResetStats();
        }


        private string GetTimeStamp()
        {
            return DateTime.Now.ToString("MM.dd.yyyy HH:mm:ss");
        }

        public void LogState(string stateName, string stateDescription, string answerType)
        {
            string d = ",";
            string line = GetTimeStamp() + d + sessionId + d + parameters.playerId + d + parameters.scenarioId + d + stateName + d +
                stateDescription + d + (totalGood + totalBad) + d + answerType + d + totalGood + d + totalBad;

            File.AppendAllText("log.csv", line + "\n");
        }

        public void ResetStats()
        {
            totalGood = 0;
            totalBad = 0;
        }

        public void IncreaseGood()
        {
            totalGood++;
        }

        public void IncreaseBad()
        {
            totalBad++;
        }


    }
}
