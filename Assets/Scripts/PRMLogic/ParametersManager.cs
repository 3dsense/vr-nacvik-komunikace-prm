using sense3d.localization;
using System;
using UnityEngine;

namespace comguide.logic.vrpolice
{
    public class ParametersManager : MonoBehaviour
    {
        private readonly string PLAYER_ID_KEY = "-player";
        private readonly string LANGUAGE_ID_KEY = "-language";
        private readonly string SCENARIO_ID_KEY = "-scenario";


        public string playerId = "defaultPlayer";
        public string language = "cz";
        public string scenarioId = "defaultScenario";


        void Awake()
        {
            ParseParameters();
            print("params " + playerId + " " + language + " " + scenarioId);
        }

        private void Start()
        {
            LocalizationManager.Get.ChangeLocalization(Enum.Parse<Localization>(language, true));
        }

        public void ParseParameters()
        {
            string[] args = System.Environment.GetCommandLineArgs();
            for (int i = 0; i < args.Length; i++)
            {
                if (args[i] == PLAYER_ID_KEY)
                {
                    playerId = args[i + 1];
                }
                if (args[i] == LANGUAGE_ID_KEY)
                {
                    language = args[i + 1];
                }
                if (args[i] == SCENARIO_ID_KEY)
                {
                    scenarioId = args[i + 1];
                }
            }
        }
    }

}
