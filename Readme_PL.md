# Wirtualna rzeczywistość jako narzędzie do szkolenia komunikacji z mniejszościami

**Autor projektu: 3dsense s.r.o., Rubešova 8, Praga 2, Republika Czeska**

## O projekcie

Projekt został stworzony w ramach grantu Erasmus+ i koncentruje się na rozwoju aplikacji wirtualnej rzeczywistości (VR) kompatybilnej ze SteamVR. Celem aplikacji jest symulacja sytuacji, w których funkcjonariusze policji muszą przekazywać poważne wiadomości członkom mniejszości, w szczególności mniejszości romskiej. Aplikacja zawiera:

- Model 3D (w tym twarz) członka mniejszości romskiej
- Kod źródłowy w C#/Unity
- Wszystkie animacje 3D nagrane przy użyciu przechwytywania ruchu (mocap)
- Ścieżki dźwiękowe odpowiadające animacjom 3D, nagrane przez aktora w języku czeskim i polskim.

Projekt jest open-source i licencjonowany na licencji MIT.

[Dalsze szczegóły i materiały do nauki](https://comguide.cz/erasmus-prm)

## Dedykacja grantu

<img src="erasmus_logo_pl.png" alt="Finansowane przez UE" width="400">

*Sfinansowane ze środków UE. Wyrażone poglądy i opinie są jedynie opiniami autora lub autorów i niekoniecznie odzwierciedlają poglądy i opinie Unii Europejskiej lub Europejskiej Agencji Wykonawczej ds. Edukacji i Kultury (EACEA). Unia Europejska ani EACEA nie ponoszą za nie odpowiedzialności.*

## Fragmenty
[Demo Video](https://vimeo.com/936621829/12fa1fa4ab?share=copy)

<img src="screenshot1.png" alt="Screenshot 1" width="600">
<img src="screenshot2.png" alt="Screenshot 2" width="600">


## Wymagania

- Komputer PC z systemem Windows 10/11, min. 16 GB pamięci RAM, karta graficzna min. Nvidia RTX 2070 8GB
- Unity 2022.1
- Zestaw VR kompatybilny ze SteamVR (HTC Vive, Oculus Quest przez kabel Oculus Link)

## Funkcje aplikacji

1. **3D Model**: Aplikacja zawiera szczegółowy model 3D członka mniejszości romskiej, który jest w pełni narigowany i gotowy do animacji.
2. **Motion Capture Animation**.
3. **Wielojęzyczny dźwięk**.
4. **Interaktywne scenariusze**.


## Autorzy

Projekt został opracowany przez **3dsense s.r.o.**, Rubešova 8, Praga 2, Republika Czeska.

---

Licencjonowany na podstawie [licencji MIT](LICENSE).

---

*Ostatnia aktualizacja: 29.5.2024*