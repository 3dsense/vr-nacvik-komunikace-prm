
# Virtuální realita jako nástroj pro nácvik komunikace s příslušníky menšin

**Autor projektu: 3dsense s.r.o., Rubešova 8, Praha 2, Česká republika**

[Polska wersja](Readme_PL.md)

## O projektu

Tento projekt vznikl jako součást grantu Erasmus+ a je zaměřen na vývoj aplikace pro virtuální realitu (VR) kompatibilní se SteamVR. Cílem aplikace je simulovat situace, ve kterých policisté musí komunikovat závažné zprávy příslušníkům menšin, konkrétně romské menšiny. Aplikace zahrnuje:

- Narigovaný 3D model (včetně obličeje) příslušníka romské menšiny
- Zdrojové kódy v C#/Unity
- Všechny 3D animace nahrané pomocí motion capture (mocap)
- Zvukové stopy odpovídající 3D animacím, nahrané hercem v českém a polském jazyce

Projekt je open-source a je licencován pod MIT licencí.

[Další podrobnosti a studijní materiály](https://comguide.cz/erasmus-prm)

## Dedikace grantu

<img src="erasmus_logo.png" alt="Financováno EU" width="400">

*Financováno Evropskou unií. Názory vyjádřené jsou názory autora a neodráží nutně oficiální stanovisko Evropské unie či Evroské výkonné agentury pro vzdělávání a kulturu (EACEA).  Evropská unie ani EACEA za vyjádřené názory nenese odpovědnost.*

## Ukázky
[Demo Video](https://vimeo.com/936586903/4fdb7b5ac2?share=copy)

<img src="screenshot1.png" alt="Screenshot 1" width="600">
<img src="screenshot2.png" alt="Screenshot 2" width="600">


## Požadavky

- PC s Windows 10/11, min. 16GB RAM, GPU min. Nvidia RTX 2070 8GB
- Unity 2022.1
- VR headset kompatibilní se SteamVR (HTC Vive, Oculus Quest přes Oculus Link kabel)

## Funkce aplikace

1. **Narigovaný model**: Aplikace obsahuje detailní 3D model příslušníka romské menšiny, který je plně narigovaný a připravený k animaci.
2. **Motion Capture animace**: Všechny animace byly nahrány pomocí profesionální technologie motion capture, což zajišťuje realistické pohyby a reakce modelu.
3. **Multijazyčný zvuk**: Zvukové stopy byly nahrány v českém a polském jazyce, aby pokryly různé jazykové potřeby uživatelů.
4. **Interaktivní scénáře**: Aplikace obsahuje interaktivní scénář, ve kterém uživatelé mohou trénovat své komunikační dovednosti při sedělování závažné zprávy.


## Autoři

Projekt vytvořila firma **3dsense s.r.o.**, Rubešova 8, Praha 2, Česká republika.

---

Licencováno pod [MIT licencí](LICENSE).

---

*Poslední aktualizace: 29.5.2024*
